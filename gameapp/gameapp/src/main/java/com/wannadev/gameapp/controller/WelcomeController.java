package com.wannadev.gameapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {

	@GetMapping("/welcome")
	public String welcome() {
		System.out.print("#######################   welcome  ############################");
		return "welcome";
	}
}
